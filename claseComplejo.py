class Complejo:
    """Representa un numero complejo
    Argumentos clave:
    a -- la parte real
    b - la parte imaginaria
    """ 

    def __init__(self, a, b):
        self.real = a
        self.img = b
    
    def suma(self, s):
        """
        Suma dos números complejos y devuelve la suma
        """
        a = self.real + s.real
        b = self.img + s.img
        return Complejo(a, b)

    def resta(self, r):
        """
        Resta dos números complejos y devuelve la resta
        """
        a = self.real - r.real
        b = self.img - r.img
        return Complejo(a, b)

    def multiplicacion(self, m):
        """
        Multiplica dos números complejos y devuelve la multiplicacion
        """
        a = (self.real*m.real) - (self.img*m.img)
        b = (self.real*m.img) + (self.img*m.real)
        return Complejo(a, b)

    def division(self, d):
        """
        Divide dos números complejos y devuelve la division
        """
        a = ((self.real*d.real) + (self.img*d.img)) / ((d.real**2) + (d.img**2))
        b = ((self.img*d.real) - (self.real*d.img)) / ((d.real**2) + (d.img**2))
        return Complejo(a, b)

    def igual(self, otro):
        """
        Explica qué significa que un número complejo sea igual a otro (que sus partes reales coincidan entres ellas y sus partes imaginarias tambien)
        """
        return self.real == otro.real and self.img == otro.img

    def conjugado(self):
        return Complejo(self.real, -self.img)