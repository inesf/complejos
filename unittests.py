import unittest
from claseComplejo import Complejo
class TestComplejo(unittest.TestCase):
        def test_suma1(self):
            c1 = Complejo(1, 2)
            c2= Complejo(3, 4)

            suma1 = c1.suma(c2)

            self.assertEqual(suma1.real, 4)
            self.assertEqual(suma1.img, 6)
        
        def test_suma2(self):
            c1 = Complejo(1, 2)
            c2= c1.conjugado()

            suma2 = c1.suma(c2)

            self.assertEqual(suma2.real, 2)
            self.assertEqual(suma2.img, 0)


        def test_resta(self):
            c1 = Complejo(3, 3)
            c2= Complejo(8, 4)

            resta = c1.resta(c2)

            self.assertEqual(resta.real, -5)
            self.assertEqual(resta.img, -1)

        def test_multiplicacion(self):
            c1 = Complejo(1, 1)
            c2= Complejo(-1, -1)

            multiplicacion = c1.multiplicacion(c2)

            self.assertEqual(multiplicacion.real, 0)
            self.assertEqual(multiplicacion.img, -2)

        def test_division(self):
            c1 = Complejo(2, 1)
            c2= Complejo(3, 2)

            division = c1.division(c2)

            self.assertEqual(division.real, 8/13)
            self.assertEqual(division.img, -1/13)

if __name__ == '__main__':
    unittest.main()